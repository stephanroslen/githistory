# git File History Browser

This utility allows you to browse through the versions of a file in your git repository.

## Prerequisites

bash and git. For best results, use an editor or IDE that automatically reloads files upon external changes.

## Installation

No installation required. Just copy `githistory.sh` somewhere and run it with bash, or put it on your $PATH and make it executable. The following examples simply assume that the file is in the current directory.

## What It Does

`githistory.sh` allows you to walk through the history of a file in your git repository: Previous version, next version, oldest version, etc. It also displays changes and log messages. To do so, it checks out the commit that contains the respective version of the file (so the other files in your clone of the repository reflect the context of that particular version). An external editor, preferably one that automatically reloads changes, is used to display the contents of the file.

## How To Use It

Pass the name of the file you want to analyse as a command line argument. Example:

```sh
$ cd your_repository
$ bash githistory.sh README.md
```

You need to start with a clean working tree (otherwise the script will abort).

Open the file in your editor or IDE. If possible, set the editor to automatically reload the file when changed externally.

The script displays the commit message of the current version of the file, plus a prompt that summarises the available commands. Enter one of the following commands (followed by Enter):

* `-`: Switch to the previous (older) version of the file.
* `+`: Switch to the next (newer) version of the file.
* `0`: Switch to the first (oldest) version of the file.
* `$`: Switch to the last (newest) version of the file.
* `l`: Display the git log of the file (up to the version that's currently displayed).
* `d`: Show the changes that lead to the currently displayed version in git diff format.
* `e`: Open the file in the editor (as specified by environment variable `EDITOR`). Close the editor to return to the prompt.
* `q`: Quit and return the repo to the version you started with. (You may also press Ctrl-C or Ctrl-D.)

You can also enter a number to jump to that version of the file (0=oldest, 1=second-oldest, etc.)

The editor or IDE will display each version of the file you're switching to.

## Practical Example

Running `githistory.sh` in the link-time plug-in repository (https://gitlab.com/wolframroesler/linktimeplugin).

```sh
$ cd linktimeplugin

$ gvim README.md

$ bash githistory.sh README.md 
commit 9b9cec20877b13fc93a3bf17ac9feacf889af9e1 (HEAD, origin/master, origin/HEAD, master)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Mon Jun 28 11:57:06 2021 +0200

    Add YouTube link to talk about link-time plug-ins
    
    held at the Osnabrück C++ user meeting on 2021-06-24.

(4/4) e edit, l log, d diff, - previous, 0 oldest, q quit > 2

commit 52085e848655967ab1b84153628d20bb99f1437e (HEAD)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Thu May 20 10:52:28 2021 +0200

    Update readme file: C++17 isn't needed, works fine in C++11

(2/4) e edit, l log, + next, $ youngest, d diff, - previous, 0 oldest, q quit > d

diff --git a/README.md b/README.md
index 05046b5..63a738b 100644
--- a/README.md
+++ b/README.md
@@ -1,6 +1,6 @@
 # Link-Time Plug-In Management
 
-C++17 header-only library for registration and management of link-time plug-ins.
+C++11 header-only library for registration and management of link-time plug-ins.
 
 ## What is it?
 
commit 52085e848655967ab1b84153628d20bb99f1437e (HEAD)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Thu May 20 10:52:28 2021 +0200

    Update readme file: C++17 isn't needed, works fine in C++11

(2/4) e edit, l log, + next, $ youngest, d diff, - previous, 0 oldest, q quit > +

commit 8bc6cc42f5c33ca08c60419031aa566778a66858 (HEAD)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Fri Jun 18 12:09:51 2021 +0200

    Split the demo program into several files

(3/4) e edit, l log, + next, $ youngest, d diff, - previous, 0 oldest, q quit > l

commit 8bc6cc42f5c33ca08c60419031aa566778a66858 (HEAD)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Fri Jun 18 12:09:51 2021 +0200

    Split the demo program into several files

commit 52085e848655967ab1b84153628d20bb99f1437e
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Thu May 20 10:52:28 2021 +0200

    Update readme file: C++17 isn't needed, works fine in C++11

commit a324cfe09bdd1b131e3bcf171ed9594f75b5e726
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Tue Mar 10 13:55:43 2020 +0100

    Initial commit
commit 8bc6cc42f5c33ca08c60419031aa566778a66858 (HEAD)
Author: Wolfram Rösler <wolfram.roesler@devolo.de>
Date:   Fri Jun 18 12:09:51 2021 +0200

    Split the demo program into several files

(3/4) e edit, l log, + next, $ youngest, d diff, - previous, 0 oldest, q quit > q
```

---
*Wolfram Rösler • wolfram@roesler-ac.de • https://gitlab.com/wolframroesler • https://twitter.com/wolframroesler • https://www.linkedin.com/in/wolframroesler/*
